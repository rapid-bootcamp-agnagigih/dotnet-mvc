﻿using Microsoft.AspNetCore.Mvc;
using WebMVC.Models;

namespace WebMVC.Controllers
{
    public class ReturnAssetController : Controller
    {
        private static List<ReturnAssetViewModel> _returnAssetViewModels = new List<ReturnAssetViewModel>()
        {
            new ReturnAssetViewModel(1, 1, "Penanggung jawab 1"),
            new ReturnAssetViewModel(2, 2, "Penanggung jawab 2"),
            new ReturnAssetViewModel(3, 3, "Penanggung jawab 1"),
            new ReturnAssetViewModel(4, 4, "Penanggung jawab 1"),
        };

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Save([Bind("Id, AssetId, PICName, SpecificationReq")] ReturnAssetViewModel returnAsset)
        {
            returnAsset.ReturnDate = DateTime.Now;
            _returnAssetViewModels.Add(returnAsset);
            return Redirect("List");
        }

        public IActionResult List()
        {
            return View(_returnAssetViewModels);
        }

        public IActionResult Edit(long? id)
        {
            // find the data with lambda
            ReturnAssetViewModel returnAsset = _returnAssetViewModels.Find(x => x.Id.Equals(id));
            return View(returnAsset);
        }

        [HttpPost]
        public IActionResult Update(long id, [Bind("Id, AssetId, PICName, SpecificationReq")] ReturnAssetViewModel returnAsset)
        {
            // delete the data
            ReturnAssetViewModel returnAssetOld = _returnAssetViewModels.Find(x => x.Id.Equals(id));
            DateTime date = returnAssetOld.ReturnDate;
            _returnAssetViewModels.Remove(returnAssetOld);

            // add new data
            returnAsset.ReturnDate = date;
            _returnAssetViewModels.Add(returnAsset);

            return Redirect("List");
        }

        public IActionResult Details(long id)
        {
            // find with linq
            ReturnAssetViewModel request = (
                from req in _returnAssetViewModels
                where req.Id.Equals(id)
                select req).SingleOrDefault(new ReturnAssetViewModel());
            return View(request);
        }

        public IActionResult Delete(long? id)
        {
            // find the data
            ReturnAssetViewModel request = _returnAssetViewModels.Find(x => x.Id.Equals(id));

            // remove from list
            _returnAssetViewModels.Remove(request);
            return Redirect("/ReturnAsset/List");
        }
    }
}
