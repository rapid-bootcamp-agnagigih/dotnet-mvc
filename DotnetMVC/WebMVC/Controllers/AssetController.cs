﻿using Microsoft.AspNetCore.Mvc;
using WebMVC.Models;

namespace WebMVC.Controllers
{
    public class AssetController : Controller
    {
        private static List<AssetViewModel> _assetViewModels = new List<AssetViewModel>()
        {
            new AssetViewModel(1, "Laptop Mainstream", 2022, "LPMS2000X", "Ryzen 5 6600U"),
            new AssetViewModel(2, "Laptop Mainstream", 2022, "LPMS2001X", "Ryzen 7 6800U"),
            new AssetViewModel(3, "Laptop Mainstream", 2022, "LPMS2002X", "Intel Core i5 1235U"),
            new AssetViewModel(4, "Laptop Mainstream", 2022, "LPMS2003X", "Intel Core i7 1265U"),
        };

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Save([Bind ("Id, Name, PurchaseYear, SerialNumber, Specification")] AssetViewModel asset)
        {
            _assetViewModels.Add(asset);
            return Redirect("List");
        }

        public IActionResult List()
        {
            return View(_assetViewModels);
        }

        public IActionResult Edit(long? id)
        {
            // find the data with lambda
            AssetViewModel asset = _assetViewModels.Find(x => x.Id.Equals(id));
            return View(asset);
        }

        [HttpPost]
        public IActionResult Update(long id, [Bind("Id, Name, PurchaseYear, SerialNumber, Specification")] AssetViewModel asset)
        {
            // delete the data
            AssetViewModel assetOld = _assetViewModels.Find(x => x.Id.Equals(id));
            _assetViewModels.Remove(assetOld);

            // add new data
            _assetViewModels.Add(asset);
            return Redirect("List");
        }

        public IActionResult Details(long id)
        {
            // find with linq
            AssetViewModel asset = (
                from a in _assetViewModels
                where a.Id.Equals(id)
                select a).SingleOrDefault(new AssetViewModel());
            return View(asset);
        }

        public IActionResult Delete(long? id)
        {
            // find the data
            AssetViewModel asset = _assetViewModels.Find(x => x.Id.Equals(id));

            // remove from list
            _assetViewModels.Remove(asset);
            return Redirect("/Asset/List");
        }
    }
}
