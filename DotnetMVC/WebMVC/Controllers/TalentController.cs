﻿using Microsoft.AspNetCore.Mvc;
using WebMVC.Models;

namespace WebMVC.Controllers
{
    public class TalentController : Controller
    {
        private static List<TalentViewModel> _talentViewModels = new List<TalentViewModel>()
        {
            new TalentViewModel(1, "Talent 1", "talent1@mail.com", "S1", "Kabupaten 1", "08-05-2000"),
            new TalentViewModel(2, "Talent 2", "talent2@mail.com", "SMA", "Kota 1", "24-03-2004"),
            new TalentViewModel(3, "Talent 3", "talent3@mail.com", "SMK", "Kabupaten 2", "09-09-2004"),
            new TalentViewModel(4, "Talent 4", "talent4@mail.com", "S2", "Kota 2", "23-11-1996"),
        };

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Save([Bind("Id, Name, Email, Education, PlaceOfBirth, DateOfBirth")] TalentViewModel talent)
        {
            _talentViewModels.Add(talent);
            return Redirect("List");
        }

        public IActionResult List()
        {
            return View(_talentViewModels);
        }

        public IActionResult Edit(long? id)
        {
            // find the data with lambda
            TalentViewModel talent = _talentViewModels.Find(x => x.Id.Equals(id));
            return View(talent);
        }

        [HttpPost]
        public IActionResult Update(long id, [Bind("Id, Name, Email, Education, PlaceOfBirth, DateOfBirth")] TalentViewModel talent)
        {
            // delete the data
            TalentViewModel talentOld = _talentViewModels.Find(x => x.Id.Equals(id));
            _talentViewModels.Remove(talentOld);

            // add new data
            _talentViewModels.Add(talent);
            return Redirect("List");
        }

        public IActionResult Details(long id)
        {
            // find with linq
            TalentViewModel talent = (
                from a in _talentViewModels
                where a.Id.Equals(id)
                select a).SingleOrDefault(new TalentViewModel());
            return View(talent);
        }

        public IActionResult Delete(long? id)
        {
            // find the data
            TalentViewModel talent = _talentViewModels.Find(x => x.Id.Equals(id));

            // remove from list
            _talentViewModels.Remove(talent);
            return Redirect("/Talent/List");
        }
    }
}
