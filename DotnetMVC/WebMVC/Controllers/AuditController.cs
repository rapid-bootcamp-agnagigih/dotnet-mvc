﻿using Microsoft.AspNetCore.Mvc;
using WebMVC.Models;

namespace WebMVC.Controllers
{
    public class AuditController : Controller
    {
        private static List<AuditViewModel> _auditViewModels = new List<AuditViewModel>()
        {
            new AuditViewModel(1, "Penanggung jawab 1", 1, true, true, true, "Normal"),
            new AuditViewModel(2, "Penanggung jawab 4", 3, true, false, true, "Baterai bocor"),
            new AuditViewModel(3, "Penanggung jawab 2", 4, true, false, true, "Normal"),
            new AuditViewModel(4, "Penanggung jawab 3", 2, true, true, false, "Layar Dead Pixel"),
        };

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Save(
            [Bind("Id, PICName, AssetId, AntiVirus, OfficeLicense, WindowsLicense, AssetCondition")] 
        AuditViewModel audit)
        {
            _auditViewModels.Add(audit);
            return Redirect("List");
        }

        public IActionResult List()
        {
            return View(_auditViewModels);
        }

        public IActionResult Edit(long? id)
        {
            // find the data with lambda
            AuditViewModel audit = _auditViewModels.Find(x => x.Id.Equals(id));
            return View(audit);
        }

        [HttpPost]
        public IActionResult Update(long id,
            [Bind("Id, PICName, AssetId, AntiVirus, OfficeLicense, WindowsLicense, AssetCondition")]
            AuditViewModel audit)
        {
            // delete the data
            AuditViewModel auditOld = _auditViewModels.Find(x => x.Id.Equals(id));
            _auditViewModels.Remove(auditOld);

            // add new data
            _auditViewModels.Add(audit);
            return Redirect("List");
        }

        public IActionResult Details(long id)
        {
            // find with linq
            AuditViewModel audit = (
                from au in _auditViewModels
                where au.Id.Equals(id)
                select au).SingleOrDefault(new AuditViewModel());
            return View(audit);
        }

        public IActionResult Delete(long? id)
        {
            // find the data
            AuditViewModel audit = _auditViewModels.Find(x => x.Id.Equals(id));

            // remove from list
            _auditViewModels.Remove(audit);
            return Redirect("/Audit/List");
        }
    }
}
