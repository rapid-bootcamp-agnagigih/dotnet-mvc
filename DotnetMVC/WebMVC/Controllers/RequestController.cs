﻿using Microsoft.AspNetCore.Mvc;
using WebMVC.Models;

namespace WebMVC.Controllers
{
    public class RequestController : Controller
    {
        private static List<RequestViewModel> _requestViewModels = new List<RequestViewModel>()
        {
            new RequestViewModel(1, 1, "Penanggung jawab 1", "Ryzen 5 atau Intel Core i5"),
            new RequestViewModel(2, 2, "Penanggung jawab 2", "Ryzen 5 atau Intel Core i5"),
            new RequestViewModel(3, 3, "Penanggung jawab 3", "Ryzen 7 atau Intel Core i7"),
            new RequestViewModel(4, 4, "Penanggung jawab 4", "Ryzen 7 atau Intel Core i7")
        };

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Save([Bind("Id, AssetId, PICName, SpecificationReq")] RequestViewModel request)
        {
            _requestViewModels.Add(request);
            return Redirect("List");
        }

        public IActionResult List()
        {
            return View(_requestViewModels);
        }

        public IActionResult Edit(long? id)
        {
            // find the data with lambda
            RequestViewModel request = _requestViewModels.Find(x=> x.Id.Equals(id));
            return View(request);
        }

        [HttpPost]
        public IActionResult Update(long id, [Bind("Id, AssetId, PICName, SpecificationReq")] RequestViewModel request)
        {
            // delete the data
            RequestViewModel requestOld = _requestViewModels.Find(x => x.Id.Equals(id));
            _requestViewModels.Remove(requestOld);

            // add new data
            _requestViewModels.Add(request);
            return Redirect("List");
        }

        public IActionResult Details(long id)
        {
            // find with linq
            RequestViewModel request = (
                from req in _requestViewModels
                where req.Id.Equals(id)
                select req).SingleOrDefault(new RequestViewModel());
            return View(request);
        }

        public IActionResult Delete(long? id)
        {
            // find the data
            RequestViewModel request = _requestViewModels.Find(x => x.Id.Equals(id));

            // remove from list
            _requestViewModels.Remove(request);
            return Redirect("/Request/List");
        }

    }
}
