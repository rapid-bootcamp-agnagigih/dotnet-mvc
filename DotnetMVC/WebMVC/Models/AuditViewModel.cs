﻿namespace WebMVC.Models
{
    public class AuditViewModel
    {
        public long Id { get; set; }
        public string PICName { get; set; }
        public long AssetId { get; set; }
        public bool AntiVirus { get; set; }
        public bool OfficeLicense { get; set; }
        public bool WindowsLicense { get; set; }
        public string AssetCondition { get; set; }

        public AuditViewModel() { }

        public AuditViewModel(long id, string picName, long assetId, bool antiVirus, bool officeLicense, bool windowsLicense, string assetCondition)
        {
            Id = id;
            PICName = picName;
            AssetId = assetId;
            AntiVirus = antiVirus;
            OfficeLicense = officeLicense;
            WindowsLicense = windowsLicense;
            AssetCondition = assetCondition;
        }
    }
}
