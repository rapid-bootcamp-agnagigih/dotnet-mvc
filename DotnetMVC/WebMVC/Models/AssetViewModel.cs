﻿namespace WebMVC.Models
{
    public class AssetViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int PurchaseYear { get; set; }
        public string SerialNumber { get; set; }
        public string Specification { get; set; }

        public AssetViewModel() { }

        public AssetViewModel(int id, string name, int purchaseYear, string serialNumber, string specification)
        {
            Id = id;
            Name = name;
            PurchaseYear = purchaseYear;
            SerialNumber = serialNumber;
            Specification = specification;
        }
    }
}
