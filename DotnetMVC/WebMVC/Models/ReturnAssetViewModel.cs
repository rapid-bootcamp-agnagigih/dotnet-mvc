﻿namespace WebMVC.Models
{
    public class ReturnAssetViewModel
    {
        public long Id { get; set; }
        public long AssetId { get; set; }
        public string PICName { get; set; }
        public DateTime ReturnDate { get; set; }

        public ReturnAssetViewModel() {
        }

        public ReturnAssetViewModel(long id, long assetId, string pICName)
        {
            Id = id;
            AssetId = assetId;
            PICName = pICName;
            ReturnDate = DateTime.Now;
        }
    }
}
