﻿namespace WebMVC.Models
{
    public class RequestViewModel
    {
        public long Id { get; set; }
        public long AssetId { get; set; }
        public string PICName { get; set; }
        public string SpecificationReq { get; set; }

        public RequestViewModel() { }

        public RequestViewModel(long id, long assetId, string pICName, string specificationReq)
        {
            Id = id;
            AssetId = assetId;
            PICName = pICName;
            SpecificationReq = specificationReq;
        }
    }
}
