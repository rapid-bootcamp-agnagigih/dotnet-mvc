﻿namespace WebMVC.Models
{
    public class TalentViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Education { get; set; }
        public string PlaceOfBirth { get; set; }
        public DateTime DateOfBirth { get; set; }

        public TalentViewModel() { }

        public TalentViewModel(long id, string name, string email, string education, string placeOfBirth, string stringDateOfBirth)
        {
            Id = id;
            Name = name;
            Email = email;
            Education = education;
            PlaceOfBirth = placeOfBirth;
            DateOfBirth = DateTime.ParseExact(stringDateOfBirth, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
        }
        public TalentViewModel(long id, string name, string email, string education, string placeOfBirth, DateTime dateOfBirth)
        {
            Id = id;
            Name = name;
            Email = email;
            Education = education;
            PlaceOfBirth = placeOfBirth;
            DateOfBirth = dateOfBirth;
        }
    }
}
